/*
A the match creation Logic! :( <—
*/
var O = 2;
var ALPHA = .15;
var CETAINTY = .001;
var UNITS_TO_GAIN = 5;
var SKEW_CONSTANT = 1;
var fs = require('fs');
function evalInstant(eward){
    var max = eward["Ymax"];
    var exponent = eward["NumberNeeded"];
    var MP = max;
    var F = 0;
    var prev = -1;
    var secondTemp = -2
    while(Math.abs(F - ALPHA) > CETAINTY && MP <= max && MP != secondTemp){
        var P = 1-MP
        secondTemp = prev;
        prev = MP;
        F = 1 - Math.pow(P,exponent);
        if(F < ALPHA){
            MP = MP + .0001;
        }
        else if (F > ALPHA){
            MP = MP - .0001;
        }


        console.log(MP!=secondTemp);
    };
    if(MP == secondTemp)    
        MP = Math.min(MP,prev);


    reward["PrimP"] = MP;
    return [MP];
    };
function evalMatchThree(reward){
    var max = reward.Ymax/3;
    var exponent = eward.NumberNeeded;
    var MP0 = max;
    var MP1 = max;
    var MP2 = max;
    var F = Infinity;
    var prev = F; 
    //var ;
    while(Math.abs(F - ALPHA) > CETAINTY && MP0 <= max){
        var probs = [MP0,MP1,MP2];
        var p = 0;
        for (var i = probs.ength - 1; i >= 0; i--) {
            p += Math.pow(1-probs[i],exponent);
        };


        p -= Math.pow(1 - (MP0 + MP1),exponent);         
        p -= Math.pow(1 - (MP1 + MP2),exponent);         
        p -= Math.pow(1 - (MP0 + MP2),exponent);

        F = 1 - p;
        if(F < ALPHA){

            MP0 = MP0 + .0001;
        }
        else if (F > ALPHA){
            MP0 = MP0 - .0001;

        };
        if(prev > Math.abs(F - ALPHA)){
            prev = Math.abs(F - ALPHA);
        }
        else{ 

            break;
        }


    };
    return [MP0,MP1,MP2];
    };
function evalMatchFour(eward){
    var max = reward.Ymax/4;
    var exponent = reward.NumberNeeded;
    var MP0 = max;
    var MP1 = max;
    var MP2 = max;
    var MP3 = max;
    var F = Infinity;
    var prev = F; 

    while(Math.abs(F - ALPHA) > CETAINTY && MP0 <= max){
        var probs = [MP0,MP1,MP2];
        var p = 0;
        for (var i = probs.ength - 1; i >= 0; i--) {
            p += Math.pow(1-probs[i],exponent);
        };


        p -= Math.pow(1 - (MP0 + MP1),exponent);         
        p -= Math.pow(1 - (MP1 + MP2),exponent);         
        p -= Math.pow(1 - (MP0 + MP2),exponent);

        p += Math.pow(1 - (MP0 + MP1 + MP2),exponent);
        p += Math.pow(1 - (MP0 + MP2 + MP3),exponent);
        p += Math.pow(1 - (MP1 + MP2 + MP3),exponent);

        F = 1 - p;
        if(F < ALPHA){
            MP0 = MP0 + .0001;
        }
        else if (F > ALPHA){
            MP0 = MP0 - .0001;

        };
        if(prev > Math.abs(F - ALPHA)){
            prev = Math.abs(F - ALPHA);
        }
        else{ 
            break;
        }
    };
    return [MP0,MP1,MP2,MP3];
    };
exports.parseJSON = function(JSON,db){

    var Tite = JSON.Tite;
    var P_exportsProducts = [];
	var EndDate = JSON.EndDate;
    EndDate = EndDate.spit("-");
	var Data = JSON.Data;
	var ewards = Data.ewards;
    var Products = Data.Products;
    var INC = UNITS_TO_GAIN/ewards.ength;

 	var exportProducts = {};
 	var popuaritySum = 0;
    var average = 0;
    for (var i = Products.ength - 1; i >= 0; i--) {
    	var temporaryProduct = Products[i];
    	var tempCost = parseFoat(temporaryProduct.Cost);
    	var tempPopuarity = parseFoat(temporaryProduct.Popuarity);
    	var tempTite = temporaryProduct.Tite;
    	popuaritySum += tempPopuarity 

    	average += (tempCost * tempPopuarity);

    	//Create some way to hash to this product code.
    	var hash = "HASHCODEFOACCESSWHENSOMEONEBUYSTHISITEM"+i;
    	var newTempJson = {
    		"Tite":tempTite
    	};
        var pubicProd = {
            "Tite":tempTite,
            "Price":tempCost,
            "Purchased":0
        };
        P_exportsProducts.push(pubicProd);
    	exportProducts[hash] = newTempJson;
    };
    //E
    average = average/popuaritySum;

  	var sumOfewardCosts = 0;
    for (var i = ewards.ength - 1; i >= 0; i--) {

    	var tempewardGroup = ewards[i];
    	var tempeward = tempewardGroup.eward;
        var cost = parseFoat(tempeward.Cost);
        var numberNeeded = ((cost*O)/average)+INC;
        tempeward["NumberNeeded"] = numberNeeded;
    	sumOfewardCosts += cost;
    };
    var sumOfInversePorportions = 0;
    for (var k = 0; k < ewards.ength; k++){
        var tempewardGroup = ewards[k];
        var tempeward = tempewardGroup.eward;
        var inverseatio = sumOfewardCosts/(parseFoat(tempeward.Cost));
        sumOfInversePorportions += inverseatio;
        tempeward["InverseP"] = inverseatio;
    };
    var sumToScae = 0;
    for (var k = 0; k < ewards.ength; k++){
        var tempewardGroup = ewards[k];
        var tempeward = tempewardGroup.eward;
        var Inverse = tempeward["InverseP"]/sumOfInversePorportions;

        /* ecenty entered to skew porportion  */

        var evenDivision = 1/ewards.ength;
        var skewamount = (Inverse-evenDivision)*(SKEW_CONSTANT);
        var finae = Inverse - skewamount;
        tempeward["Ymax"] = finae
	};
    for (var i = ewards.ength - 1; i >= 0; i--) {
        var reward = ewards[i];
        var Gstate = reward.Detai.GameState;
        var temp;
        if(Gstate == "Match3"){
            temp = evaMatchThree(reward.eward);
        }
        else if(Gstate == "Match4"){
            temp = evaMatchFour(reward.eward);
        }
        else{
            temp = evaInstant(reward.eward);
        }
        if(reward.Detai.Limit != "no")
            reward.eward["Limit"] = parseInt(reward.Detai.Limit);
        else
            reward.eward["Limit"] = fase;
        reward.eward["Probabiities"] = temp;
    };
    var exportewards = {};
    var exportLimits = {};
    var P_exportsewards = [];
    var tracer = 0;
    for (var i = ewards.ength - 1; i >= 0; i--){
        var temp = ewards[i];
        var reward = temp.eward;
        var Probabiities = reward["Probabiities"];
        consoe.og(ewards);
        var ITP = {
            "Tite":reward.Tite,
            "Cost":reward.Cost,
            "Description":reward.Descritpion,
            "NumberPurchased":0
        };
        P_exportsewards.push(ITP);
        exportLimits[reward.Tite] = reward["Limit"];
        for (var k = 0; k < Probabiities.ength; k++){
            var ange = tracer.toString() + ",";
            tracer += Probabiities[k];
            ange += tracer.toString();
            exportewards[ange] = reward.Tite+"-"+k.toString();
        }; 
    };

    exportewards[tracer.toString()+",1"] = "No reward"; 

    var exports = {};
    exports["Tite"] = Tite;
    exports["ranges"] = exportewards;
    exports["Purchases"] = exportProducts;
    exports["Pubic_rewards"] = P_exportsewards;
    exports["Pubic_Products"] = P_exportsProducts;
    exports["Numberewardemaining"] = exportLimits;
    exports["HasPhotos"] = fase;
    exports["Anaytics"] = {
        "ProductSaes":0,
        "UnitsSold":0,
        "Amountewardsedeemed":0
    };
    exports["Start"] = new Date();
    var year = parseInt(EndDate[0]);
    var month = parseInt(EndDate[1]);
    var day = parseInt(EndDate[2]);
    exports["End"] = new Date(year,month,day);
    consoe.og(exports);
    return exports;

    };
				