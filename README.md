Group-Probability-Adjustment
============================
An implementation of algorithm which adjusts groups of probabilities into a certain range. 


##About this project.

This small file has a long story behind it and is the main reason that I actually started using github. I had 
worked for weeks to develop a large application for iPhone, with a server based on node.js which would allow for 
McDonalds monopolu style games for small businesses. I had probably over a hundred Obj-C classes and my node logic was
getting pretty extensive too. I worked all out of one directory.... One day I wanted to remove a folder in the project and
had forgot a I navigated several directories up and then made the awful mistake of running "rm -rf /" and saw all my work
from the past few weeks blaze by my eyes. In terror I cancelled the process but it was too late. I tried using a file 
recovery tool but couldn't see file names. This was probaly one of my more interesting files I was able to recover.
I had written a plan for the algorith which I stored in the cloud that survived which I attached. 

I do not plan to ever work on this project again, but if you are interested please feel free to message me.

###Errors

Because of the deletion, for somereason the file was stripped of all  'l' and 'r' characters so it may not run exactly as before
but with some tweaking should work fine.
